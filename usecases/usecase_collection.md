# Use Case 'Collection'

For more see ['main' README.md](../README.md).

**Case** for **several inputs**, gathering **outputs into a collection**,
and also inputs into another one :

  - What for :
    - Input collection and output collection are here used to be able to
      **get some datasets information** (input datasets, output datasets)
      by a single request (instead of one request for each dataset).
    - Output collection is here used to **download** (as a .zip file) several
      outputs by a single request (instead of one request for each output
      dataset).
    - Collection are **NOT used** here to upload inputs neither to run.

  - **Sequence** :
    - Create a history (=> **history_id**)
    - Upload the input .zip files (=> **input_data_info_list**)
    - Create a collection from input_data_info_list
      (=> **input_dataset_collection_info**)
    - Run for each input (=> **output_data_info_list**)
    - Create a collection from output_data_info_list
      (=> **output_dataset_collection_info**)
    - Survey (=> **output_dataset_collection_info** refreshed)
    - Download (=> One .zip **dataset_collection_filepath**)
      - Note : the N .RData result files could be downloaded one by one 
        (N requests, by calling 'saveResults' method for each output dataset
        of output_data_info_list). See
        '[Use Case 'Multiple'](./usecase_multiple.md)'.

  - *[Siwaa vocabulary : One tool 'Execute' for each of N 'dataset']*.

  - Prerequisite :

    - An input has been split as N **existing** inputs (.zip files), that
      are together into an input folder. In this example :
      '[../example/INPUTS/multiple](../example/INPUTS/multiple)' folder
      containing 9 .zip input files.

    - an **existing** folder where the result (1 .zip file containing some
      .RData files) will be downloaded.
      In this example : 'example/OUTPUTS' folder.

    - an **available** Siwaa API_KEY value
      (*[help](https://forgemia.inra.fr/nathalie.rousse/use/-/blob/siwaa-api/token/README.md)*).

    - For more see [example](example) folder.

```R
library('carboseqsiwaa')

#------------------------------------------------------------------------------
# Init
siwaaenv = createSiwaaEnv(API_KEY='5b54a1xxxxxxxxxxxxxxxxxxxx4e7286',
             CARBOSEQ_TOOL_ID='toolshed-siwaa.toulouse.inrae.fr/repos/patrick_chabrier/carboseq_s/CarboSeqSimulator/1.0.8',
             HISTORY_NAME='[usecase_collection.md] Use Case Collection',
             INPUT_DIR='example/INPUTS/multiple',
             OUTPUT_DIR='example/OUTPUTS')
#siwaaenv = setSiwaaEnv(env=siwaaenv, VERBOSE=TRUE)

# Upload the .zip input files (into a new created history)
ret <- uploadAllZips(env=siwaaenv)
input_data_info_list <- ret$input_data_info_list
message <- ret$message

# Create a collection gathering inputs => input_dataset_collection_info
ret = makeDatasetCollection(env=siwaaenv,
                            data_info_list=input_data_info_list,
                            dataset_collection_name='INPUT collection list')
input_dataset_collection_info = ret$dataset_collection_info

# [Optional] to observe input_dataset_collection_info (inputs) state :
if (FALSE){
    ret = updateDatasetCollectionInformation(env=siwaaenv,
                        dataset_collection_info=input_dataset_collection_info)
    input_dataset_collection_info = ret$input_dataset_collection_info
    total_end = ret$total_end
    all_data_ready = ret$all_data_ready
}

# Run => output_data_info_list (used to survey)
ret = runAllSims(env=siwaaenv, input_data_info_list=input_data_info_list)
output_data_info_list = ret$output_data_info_list
message = ret$message

# Create a collection gathering outputs => output_dataset_collection_info
# (used to survey)
ret = makeDatasetCollection(env=siwaaenv,
                            data_info_list=output_data_info_list,
                            dataset_collection_name='OUTPUT collection list')
output_dataset_collection_info = ret$dataset_collection_info

# [Optional] Ids to then more outputs observations :
if (FALSE){
    ret = idsInfoDatasetCollection(env=siwaaenv,
                       dataset_collection_info=output_dataset_collection_info)
    output_data_id_list = ret$data_id_list
    # will be used to follow/get jobs information
    output_data_job_id_list = ret$data_job_id_list
}

# Survey => output_dataset_collection_info refreshed (used to download)
ret = waitAllSimsResults(env=siwaaenv,
                       dataset_collection_info=output_dataset_collection_info,
                       delay=10, duration_max=30)
output_dataset_collection_info = ret$dataset_collection_info
                  # DON'T FORGET to refresh output_dataset_collection_info !!!
total_end = ret$total_end
all_data_ready = ret$all_data_ready
data_state_list = ret$data_state_list

# [Optional] to know more about output_dataset_collection_info (outputs) jobs :
if (FALSE){
    job_report_list = getJobReportList(env=siwaaenv,
                                       job_id_list=output_data_job_id_list)
}

# At this step, results can be downloaded as a single .zip file,
# corresponding with the output collection :
# - Results into the .zip file will be stable only if total_end.
# - Results into the .zip file will be complete only
#   if total_end and all_data_ready.

# Download files results as a single .zip file => dataset_collection_filepath
# The .zip file contains the .RData result file of each simulation for
# which end and data ready.
ret = saveResultsAllTogether(env=siwaaenv,
                        dataset_collection_info=output_dataset_collection_info)
dataset_collection_filepath = ret$dataset_collection_filepath
message = ret$message

# Clear History
#ret <- clearHistory(env=siwaaenv, data_info=input_dataset_collection_info)
ret <- clearHistory(env=siwaaenv, data_info=output_dataset_collection_info)
history_id <- ret$history_id
history_name <- ret$history_name
purged <- ret$purged
deleted <- ret$deleted
message <- ret$message

```

