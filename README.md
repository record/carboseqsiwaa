# R package 'carboseqsiwaa'

The **'carboseqsiwaa'** R package provides an API to help using the 
**'CarboSeqSimulator'** Siwaa tool or the **'CarboSeqRun'** Siwaa workflow.
It allows to **upload** input files, **run** the tool or the workflow,
**survey** the running progression, and finally **download** output files.

# Install R package

### Install from the Git repository

```
R -e "remotes::install_gitlab(repo='record/carboseqsiwaa', host='forgemia.inra.fr')"
```

### Install from R package .tar.gz file

By this way, you will have local access to 'man' doc.

R package 'carboseqsiwaa' has been deployed as a .tar.gz file, downloadable
from **https://record.pages.mia.inra.fr/carboseqsiwaa**,
with corresponding documentation (as .pdf file and online documentation).

After having downloaded the carboseqsiwaa *-bbb* .tar.gz file
*of branch 'bbb'*, **install** the R package :
```R
install.packages('path-to-carboseqsiwaa-bbb.tar.gz')
```

Example 'main' branch :
```
# download R package .tar.gz file
wget –-no-check-certificate https://record.pages.mia.inra.fr/carboseqsiwaa/main/carboseqsiwaa-main.tar.gz 
# install
Rscript -e 'install.packages("carboseqsiwaa-main.tar.gz")'
# download .pdf documentation file
wget –-no-check-certificate https://record.pages.mia.inra.fr/carboseqsiwaa/main/carboseqsiwaa-main.pdf
```

Example 'dev' branch :
```
wget –-no-check-certificate https://record.pages.mia.inra.fr/carboseqsiwaa/dev/carboseqsiwaa-dev.tar.gz 
Rscript -e 'install.packages("carboseqsiwaa-dev.tar.gz")'
wget –-no-check-certificate https://record.pages.mia.inra.fr/carboseqsiwaa/dev/carboseqsiwaa-dev.pdf
```

# Documentation

- Package documentation (case of R package installed from .tar.gz file) :
  ```R
  ls("package:carboseqsiwaa")
  ```

- Online documentation : 
  https://record.pages.mia.inra.fr/carboseqsiwaa 

- Documentation as .pdf file : 
  https://record.pages.mia.inra.fr/carboseqsiwaa 

- **More** :

  - Configuration :
    - **Siwaa environment** : mandatory.  Created with 'createSiwaaEnv' method.
    - Siwaa environment content
      (API_KEY, CARBOSEQ_TOOL_ID, CARBOSEQ_WORKFLOW_ID...)
      can be set with 'createSiwaaEnv' creation method or after creation
      with 'setSiwaaEnv' method.

  - Token :
    - Siwaa environment must contain an available Siwaa **API_KEY** value (see
    '[Token](https://forgemia.inra.fr/nathalie.rousse/use/-/blob/siwaa-api/token/README.md)').

  - Siwaa tool id : 
    - The tool id value depends on the tool version.
    - How to find the tool id value :
      in the list of tools https://siwaa.toulouse.inrae.fr/api/tools,
      search the tool with name valuing 'CarboSeqSimulator' 
      and take its 'id' as CARBOSEQ_TOOL_ID value.
    - The tool id can be given :
      - into Siwaa environment by **CARBOSEQ_TOOL_ID** value, set with
        'createSiwaaEnv' or 'setSiwaaEnv' methods.
      - as '**tool_id**' parameter of 'runSims' method.

  - Input :

    - Input is a folder containing the .csv files.
    - The input folder path can be given :
      - into Siwaa environment by **INPUT_DIR** value, set with
        'createSiwaaEnv' or 'setSiwaaEnv' methods.
      - as '**input_dir**' parameter of 'uploadCSVs' method.

  - Output :

    - The path of output folder (destination of downloaded file) can be given :
      - into Siwaa environment by **OUTPUT_DIR** value, set with
        'createSiwaaEnv' or 'setSiwaaEnv' methods.
      - as '**output_dir**' parameter of 'saveResults' method.

  - Verbose/Silent :

    - The display mode (silent/verbose) is defined by **VERBOSE**
      into Siwaa environment.
    - By default : silent mode (VERBOSE valuing FALSE).
      To activate 'verbose' mode, set VERBOSE at TRUE value, with
      'createSiwaaEnv' or 'setSiwaaEnv' methods.

    - For running progression messages to be displayed (while
      'waitSimsResults' method), activate 'verbose' mode (VERBOSE valuing TRUE).

  - Survey :

    - 'waitSimsResults' method :

      - The 'waitSimsResults' parameters '**delay**' and '**duration_max**'
        can also be set into Siwaa environment (**DELAY**, **DURATION_MAX**)
        with 'createSiwaaEnv' or 'setSiwaaEnv' methods.

      - duration_max :
        - Someone can choose to call 'waitSimsResults' with 'high' duration_max
          value (high enough), in order to have end of running at the end of
          'waitSimsResults'.
        - Someone can choose to call 'waitSimsResults' with 'low' duration_max
          value, in order to keep control while running (have choice between
          calling 'waitSimsResults' again immediatly or later on).

      - '**with_job_report** parameter of 'waitSimsResults' method allows to
        return some job report (information and metrics).

      - For running progression messages to be displayed (while
        'waitSimsResults' method), activate 'verbose' mode (VERBOSE valuing
        TRUE).

  - Some other help :

    - Request for list of histories :
      https://siwaa.toulouse.inrae.fr/api/histories
    - Request for list of tools :
      https://siwaa.toulouse.inrae.fr/api/tools
    - Request for list of workflows :
      https://siwaa.toulouse.inrae.fr/api/workflows
    - Request for list of workflow invocations :
      https://siwaa.toulouse.inrae.fr/api/invocations
    - **How to get an available API KEY value** : see
      '[Token](https://forgemia.inra.fr/nathalie.rousse/use/-/blob/siwaa-api/token/README.md)'.

# Use Cases

- **Check communication with Siwaa** (verify API_KEY value...) :

  **Code** :
  ```R
  library(carboseqsiwaa)
  siwaaenv = createSiwaaEnv(API_KEY='5b54a1xxxxxxxxxxxxxxxxxxxx4e7286')
  ret = ControlCommunicationWithSiwaa(env=siwaaenv)
  check_ok = ret$check_ok
  message = ret$message
  cat("check_ok: ", check_ok, " ** ", "message: ", message, "\n")

  # If not check_ok, then you can not continue :
  if (!check_ok){
      report = ret$report
  }
  # If check_ok, then you can continue.
  ```

- **Workflow** :
  - Sequence : upload 1 input, run workflow, download the output.
  - *[Siwaa vocabulary : One workflow 'Run' for a 'Single dataset']*.

  - **[Use Case 'Workflow' (usecase_workflow.md)](./usecases/usecase_workflow.md)** :
    - Call of the 'runWorkflow' method that launches the workflow invocation
      and then wait (infinity) for the Output data to be identified.
    - In that Use Case, user pending until workflow invocation has
      reached a terminal state.

  - **[Use Case 'Workflow' (usecase_workflow_2.md)](./usecases/usecase_workflow_2.md)** :
    - Call of the 'launchWorkflow' method that returns immediatly after having
      launched the workflow invocation.
    - Then call of the 'waitWorkflowOutputDataIdent' method that waits (loop
      limited by a duration_max) for the Output data to be identified.
    - In that Use Case, the user can choose when and how to survey workflow
      invocation state (many calls of 'waitWorkflowOutputDataIdent, choice of
      'duration_max' input parameter value...).

- **Tool** :

  - **[Use Case 'Single'](./usecases/usecase_single.md)** :
    - Sequence : upload 1 input, run tool, download the output.
    - *[Siwaa vocabulary : One tool 'Execute' for a 'Single dataset']*.

  - *Less useful since [Use Case 'Workflow'](./usecases/usecase_workflow.md)*
    *has been written:*
    - [Use Case 'Multiple'](./usecases/usecase_multiple.md) :
      Code for several inputs
    - [Use Case 'Collection'](./usecases/usecase_collection.md) :
      Code for several inputs, gathering outputs into a collection
      (and also inputs into another collection)

# Use Cases as Jupyter Notebooks

- **[SimpleCarboseqSyncJob.ipynb](./notebooksGallery/SimpleCarboseqSyncJob.ipynb.md)**
- **[SimpleCarboseqSyncJobWF.ipynb](./notebooksGallery/SimpleCarboseqSyncJobWF.ipynb)**

