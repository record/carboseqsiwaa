################################################################################
# simple-cli
#
# Simple cli commands for a very simple usage
#
################################################################################

#' Launch a plan of simulation on a cluster
#'
#' @param INPUT_DIR the path to the folder where to find the csv input files
#' @param chunck_size the size of the chunk of simulations
#' @param VERBOSE to get feedback or not
#'
#' @return res the result of the plan of simulations
#' @export
#'
simpleBatchRunSim <- function(API_KEY = Sys.getenv("CARBOSEQ_SIWAA_TOKEN"),
                              INPUT_DIR,
                              chunck_size,
                              max_chunck_size = 10000,
                              VERBOSE = TRUE) {
  if (missing(chunck_size)) {
    maxID <- max(read.csv(file.path(INPUT_DIR, 'soil.csv'))$ID)
    if (max_chunck_size > maxID)
      chunck_size = maxID
    else
      chunck_size = max_chunck_size
  }

  simpleInfinity <- 864000

  trying <- FALSE

  carboseqSIWAAdir <- Sys.getenv("CARBOSEQ_SIWAA_DIR")
  if (carboseqSIWAAdir == "")
    carboseqSIWAAdir <- file.path(Sys.getenv("HOME"), "carboseqsiwaa")
  if (!dir.exists(carboseqSIWAAdir))
    dir.create(carboseqSIWAAdir, recursive = TRUE)

  carboseqSIWAAenvFile <- file.path(carboseqSIWAAdir,
                                    "carboseqSIWAAenvFile.RData")

  carboseqSIWAAoutputDir <- file.path(carboseqSIWAAdir, "output")
  if (!dir.exists(carboseqSIWAAoutputDir))
    dir.create(carboseqSIWAAoutputDir, recursive = TRUE)

  localCarboseqSIWAAenv <- createSiwaaEnv(
    HISTORY_NAME = 'Carboseq',
    SERVER = 'http://nginx-siwaa',
    API_KEY = API_KEY,
    CARBOSEQ_WORKFLOW_ID = '32c9385265d8ab1a',
    INPUT_DIR = INPUT_DIR,
    OUTPUT_DIR = carboseqSIWAAoutputDir,
    DURATION_MAX = simpleInfinity,
    INVOCATION_DURATION_MAX = simpleInfinity,
    VERBOSE = VERBOSE
  )
  localCarboseqSIWAAenv$uploaded_data <- FALSE
  localCarboseqSIWAAenv$job_started_first_stage <- FALSE
  localCarboseqSIWAAenv$job_started_second_stage <- FALSE
  localCarboseqSIWAAenv$job_finished <- FALSE
  localCarboseqSIWAAenv$retrieven_data <- FALSE

  if (!exists("carboseqSIWAAenv")) {
    if (file.exists(carboseqSIWAAenvFile)) {
      load(carboseqSIWAAenvFile)
      tmp <- carboseqSIWAAenv
      rm(carboseqSIWAAenv)
      carboseqSIWAAenv <<- tmp
      rm(tmp)
      if (carboseqSIWAAenv$retrieven_data) {
        rm("carboseqSIWAAenv", pos = ".GlobalEnv")
        carboseqSIWAAenv <<- localCarboseqSIWAAenv
        save("carboseqSIWAAenv", file = carboseqSIWAAenvFile)
      } else  {
        cat(paste0("[Simple] Trying to get the results from previous ",
        "job first, you will need to resubmit after all")
        )
        trying <- TRUE
      }
    } else {
      carboseqSIWAAenv <<- localCarboseqSIWAAenv
      save("carboseqSIWAAenv", file = carboseqSIWAAenvFile)
    }
  } else {
    if (carboseqSIWAAenv$retrieven_data) {
      carboseqSIWAAenv <<- localCarboseqSIWAAenv
      save("carboseqSIWAAenv", file = carboseqSIWAAenvFile)
    } else {
      cat(paste0("[Simple] Trying to get the results from previous job first,",
        "you will need to resubmit after all")
      )
      trying <- TRUE
    }
  }

  if (!carboseqSIWAAenv$uploaded_data) {
    if (trying)
      cat("[Simple] Uploading data for previous batch!")
    ret <- uploadCSVs(env = carboseqSIWAAenv)
    if (exists('input_data_info', where = ret)) {
      carboseqSIWAAenv$input_data_info <<- ret$input_data_info
      carboseqSIWAAenv$uploaded_data <<- TRUE
      save("carboseqSIWAAenv", file = carboseqSIWAAenvFile)
    } else {
      cat("[Simple] Something went wrong when uploading data!")
      cat(ret$message)
      carboseqSIWAAenv$retrieven_data <<- TRUE
      return()
    }
  }

  if (!carboseqSIWAAenv$job_started_first_stage) {
    if (trying)
      cat("[Simple] Relaunching previous batch (first stage)!")

    ret <- launchWorkflow(
      env = carboseqSIWAAenv,
      input_data_info =
        carboseqSIWAAenv$input_data_info,
      chunck_size = toString(chunck_size)
    )

    carboseqSIWAAenv$invocation_id <<- ret$invocation_id
    carboseqSIWAAenv$job_started_first_stage <<- TRUE
    save("carboseqSIWAAenv", file = carboseqSIWAAenvFile)
  }

  if (!carboseqSIWAAenv$job_started_second_stage) {
    if (trying)
      cat("[Simple] Relaunching previous batch (second stage)!")

    ret <- waitWorkflowOutputDataIdent(env = carboseqSIWAAenv,
                                       invocation_id =
                                         carboseqSIWAAenv$invocation_id)

    if (ret$output_data_identified) {
      carboseqSIWAAenv$output_data_info <<- ret$output_data_info
      carboseqSIWAAenv$job_started <<- TRUE
      save("carboseqSIWAAenv", file = carboseqSIWAAenvFile)
    } else  {
      cat ("[Simple] Something went wrong when submitting job (second_stage)!")
      cat (ret$message)
      carboseqSIWAAenv$retrieven_data <<- TRUE
      return()
    }
  }

  if (!carboseqSIWAAenv$job_finished) {
    if (trying)
      cat("[Simple] Waiting for previous batch to finish!")

    ret <- waitSimsResults(env = carboseqSIWAAenv,
                           data_info = carboseqSIWAAenv$output_data_info)

    if (ret$end && ret$data_ready) {
      carboseqSIWAAenv$output_data_info <<- ret$data_info
      carboseqSIWAAenv$job_finished <<- TRUE
      save("carboseqSIWAAenv", file = carboseqSIWAAenvFile)
    } else {
      cat("[Simple] Something went wrong when awaiting the job to finish!")
      cat(ret$message)
      carboseqSIWAAenv$retrieven_data <<- TRUE
      return()
    }
  }

  if (!carboseqSIWAAenv$retrieven_data) {
    if (trying)
      cat("[Simple] retrieving data from previous batch!")
    ret <- saveResults(env = carboseqSIWAAenv,
                       data_info = carboseqSIWAAenv$output_data_info)

    if (ret$filepath != "") {
      carboseqSIWAAenv$output_file_path <<- ret$filepath
      save("carboseqSIWAAenv", file = carboseqSIWAAenvFile)
      load(carboseqSIWAAenv$output_file_path)
      carboseqSIWAAenv$retrieven_data <<- TRUE
      save("carboseqSIWAAenv", file = carboseqSIWAAenvFile)
      return(res)
    } else {
      cat("[Simple]Something went wrong when retrieving the output data !")
      cat(ret$message)
      carboseqSIWAAenv$retrieven_data <<- TRUE
      return()
    }
  }
}
