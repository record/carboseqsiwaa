#!/bin/bash

###############################################################################
# Build _'BRANCHNAME'_ folder for branch 'BRANCHNAME'
# (+ home.html)
###############################################################################

BRANCHNAME=$1

mkdir _${BRANCHNAME}_ # artifact
mkdir _${BRANCHNAME}_build

# build
cd _${BRANCHNAME}_build
git clone "https://forgemia.inra.fr/record/carboseqsiwaa.git"
cd carboseqsiwaa
git checkout ${BRANCHNAME}
Rscript -e 'install.packages(c("rjson", "httr"))'
Rscript -e "devtools::document(roclets = c('rd', 'collate', 'namespace'))"
Rscript -e "devtools::build(vignettes = TRUE, manual = TRUE)"
Rscript -e "devtools::build_site()"
Rscript -e "devtools::build_manual()"

# save
cp $(ls -rt ../*_*.tar.gz |tail -1) ../../_${BRANCHNAME}_/carboseqsiwaa-${BRANCHNAME}.tar.gz
cp $(ls -rt ../*_*.pdf |tail -1) ../../_${BRANCHNAME}_/carboseqsiwaa-${BRANCHNAME}.pdf
mv docs/reference ../../_${BRANCHNAME}_/.
git rev-parse --short HEAD > ../../_${BRANCHNAME}_/commit_SHA
cp -n docs/home.html ../../home.html
cd ../..

# clean
rm -fr _${BRANCHNAME}_build

