# Use Case 'Multiple'

For more see ['main' README.md](../README.md).

**Case** for **Several inputs** :

  - **Sequence** for **Several inputs** :
    - Create a history (=> **history_id**)
    - Loop Uploading the input .zip files (=> **input_data_info_list**)
    - Loop Running (=> **output_data_info_list**)
    - The following part (survey, downlaod) can be done by using collection
      (see [Use Case 'Collection'](usecase_collection.md) :
        - Loop Surveying (=> **output_data_info_list** refreshed)
        - Loop Downloading (=> **output_file_list**)
          - output_file_list element : output file path if downloaded,
            "" if not downloaded

  - *[Siwaa vocabulary : One tool 'Execute' for each of N 'dataset']*.

  - Prerequisite :

    - An input has been splitted as N inputs (.zip files).
      In this example, the N .zip files gathered into 'example/INPUTS/multiple'
      folder.

      See [../example/INPUTS/multiple](../example/INPUTS/multiple) folder
      content : 9 .zip input files.

    - an **existing** folder where the N results (N .RData files) will be
      downloaded (into created subfolders).
      In this example : 'example/OUTPUTS' folder.

    - an **available** Siwaa API_KEY value
      (*[help](https://forgemia.inra.fr/nathalie.rousse/use/-/blob/siwaa-api/token/README.md)*).

    - For more see [example](example) folder.

```R
library('carboseqsiwaa')

#------------------------------------------------------------------------------
# Init
#------------------------------------------------------------------------------
siwaaenv = createSiwaaEnv(API_KEY='5b54a1xxxxxxxxxxxxxxxxxxxx4e7286',
              CARBOSEQ_TOOL_ID='toolshed-siwaa.toulouse.inrae.fr/repos/patrick_chabrier/carboseq_s/CarboSeqSimulator/1.0.8')
server = getServerValue(env=siwaaenv)
api_key = getApiKeyValue(env=siwaaenv)

input_zip_list <- list.files("example/INPUTS/multiple", pattern = ".zip",
                             full.names = TRUE)

input_data_info_list = list()
output_data_info_list = list()
output_file_list = list()

# Create history => history_id (used to upload)
history_id = createHistory(
                       history_name="[usecase_multiple.md] Use Case Multiple",
                       server=server, api_key=api_key)

#------------------------------------------------------------------------------
# Upload the input .zip files => input_data_info_list (used to run)
#------------------------------------------------------------------------------
for (i in 1:length(input_zip_list)){
    input_zip = input_zip_list[[i]]
    input_data_id <- uploadFile(history_id=history_id, file_path=input_zip,
                                server=server, api_key=api_key)
    input_data_info <- getDataInfo(data_id=input_data_id,
                                   server=server, api_key=api_key)
    input_data_info_list[[i]] = input_data_info

    # Note : possible to run here, just after upload
    # ret = runSims(env=siwaaenv, input_data_info=input_data_info, v=FALSE)
    # output_data_info = ret$output_data_info
    # output_data_info_list[[i]] = output_data_info
}

#------------------------------------------------------------------------------
# Run => output_data_info_list (used to survey)
#------------------------------------------------------------------------------
# Run (if not already : see commented code)
for (i in 1:length(input_data_info_list)){
    ret = runSims(env=siwaaenv, input_data_info=input_data_info, v=FALSE)
    output_data_info = ret$output_data_info
    output_data_info_list[[i]] = output_data_info
}

#------------------------------------------------------------------------------
# Survey => output_data_info_list refreshed (used to download)
#------------------------------------------------------------------------------
total_end = TRUE
all_data_ready = TRUE

for (i in 1:length(output_data_info_list)){
    output_data_info = output_data_info_list[[i]]
    ret = waitSimsResults(env=siwaaenv, data_info=output_data_info,
                          duration_max=120, # seconds
                          with_job_report=TRUE)
     output_data_info = ret$data_info
     end <- ret$end
     data_ready <- ret$data_ready
     job_info <- ret$job_info        #!!! (unused)
     job_metrics <- ret$job_metrics  #!!! (unused)

     if (!end) total_end = FALSE
     if (!data_ready) all_data_ready = FALSE

     output_data_info_list[[i]] = output_data_info
}

#------------------------------------------------------------------------------
# Download files results (into separated subfolders) => output_file_list
#------------------------------------------------------------------------------
for (i in 1:length(output_data_info_list)){
    output_data_info = output_data_info_list[[i]]
    output_dir = paste("example/OUTPUTS", "/", "__", i, "__", output_data_info$id, "__", sep="")
    dir.create(output_dir)
    ret = saveResults(env=siwaaenv, data_info=output_data_info,
                      output_dir=output_dir)
    output_file_list[[i]] = ret$filepath
}

```

