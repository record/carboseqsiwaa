# Use Case 'Single'

For more see ['main' README.md](../README.md).

**Case** :

  - Sequence : upload 1 input, run tool, download the output.
  - *[Siwaa vocabulary : One tool 'Execute' for a 'Single dataset']*.
  - Prerequisite :
    - an **existing** folder containing the .csv input files.
      In this example : 'example/INPUTS/40Sites/csvdir' folder.
    - an **existing** folder where result (.RData file) will be downloaded.
      In this example : 'example/OUTPUTS' folder.
    - an **available** Siwaa API_KEY value
      (*[help](https://forgemia.inra.fr/nathalie.rousse/use/-/blob/siwaa-api/token/README.md)*).
    - For more see [example](example) folder.

**Code** :

```R
library('carboseqsiwaa')

siwaaenv = createSiwaaEnv(API_KEY='5b54a1xxxxxxxxxxxxxxxxxxxx4e7286',
              CARBOSEQ_TOOL_ID='toolshed-siwaa.toulouse.inrae.fr/repos/patrick_chabrier/carboseq_s/CarboSeqSimulator/1.0.8',
              HISTORY_NAME='[usecase_single.md] Use Case Single',
              INPUT_DIR='example/INPUTS/40Sites/csvdir',
              OUTPUT_DIR='example/OUTPUTS')
#siwaaenv = setSiwaaEnv(env=siwaaenv, VERBOSE=TRUE)

ret <- uploadCSVs(env=siwaaenv)
input_data_info <- ret$input_data_info
message <- ret$message

ret <- runSims(env=siwaaenv, input_data_info=input_data_info)
output_data_info <- ret$output_data_info
message <- ret$message

ret = waitSimsResults(env=siwaaenv, data_info=output_data_info)
output_data_info = ret$data_info # DON'T FORGET to refresh output_data_info !!!
message <- ret$message
end <- ret$end
data_ready <- ret$data_ready

cat("end: ", end, " ** ", "data_ready: ", data_ready, "\n")

# At this step :
# -  If end and not data_ready, then STOP.
# -  If not end, then you can call again 'wait_sim_results' to continue survey
# -  If end and data_ready, then you can download results.

# Download the output data file
ret <- saveResults(env=siwaaenv, data_info=output_data_info)
output_file_path <- ret$filepath
message <- ret$message

if (output_file_path != "") cat("OK") else cat("NOT OK")

# Verify downloaded .RData file
if (output_file_path != ""){
    load(output_file_path)
    lattice::xyplot(SOC~DATE, groups = ID, data = res$SOC, type = "l")
}

ret <- clearHistory(env=siwaaenv, data_info=output_data_info)
history_id <- ret$history_id
history_name <- ret$history_name
purged <- ret$purged
deleted <- ret$deleted
message <- ret$message
```

