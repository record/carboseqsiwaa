# Use Case 'Workflow' (2nd)

For more see ['main' README.md](../README.md).

Same as [usecase_workflow.md](./usecase_workflow.md) excepted that here,
by calling 'launchWorkflow' and 'waitWorkflowOutputDataIdent' methods,
**the user can choose when and how to survey workflow invocation state** :
many calls of 'waitWorkflowOutputDataIdent, choice of 'duration_max'
input parameter value...

**Case** :

  - Sequence : upload 1 input, run workflow, download the output.
  - *[Siwaa vocabulary : One workflow 'Run' for a 'Single dataset']*.
  - Prerequisite :
    - an **existing** folder containing the .csv input files.
      In this example : 'example/INPUTS/400Sites' folder.
    - an **existing** folder where result (.RData file) will be downloaded.
      In this example : 'example/OUTPUTS' folder.
    - an **available** Siwaa API_KEY value
      (*[help](https://forgemia.inra.fr/nathalie.rousse/use/-/blob/siwaa-api/token/README.md)*).
    - For more see [example](example) folder.

**Code** :

```R
library(carboseqsiwaa)

siwaaenv = createSiwaaEnv(API_KEY='5b54a1xxxxxxxxxxxxxxxxxxxx4e7286',
              CARBOSEQ_WORKFLOW_ID='32c9385265d8ab1a',
              HISTORY_NAME='[usecase_workflow.md] Use Case Workflow',
              INPUT_DIR='example/INPUTS/400Sites',
              OUTPUT_DIR='example/OUTPUTS')
#siwaaenv = setSiwaaEnv(env=siwaaenv, VERBOSE=TRUE)

ret <- uploadCSVs(env=siwaaenv)
input_data_info <- ret$input_data_info
message <- ret$message

# launchWorkflow launch workflow and return immediatly. Does not wait for 
# workflow invocation to be in a terminal state (that will be done after by
# waitWorkflowOutputDataIdent).
# chunck_size value should depend on input_data_info size.
ret <- launchWorkflow(env=siwaaenv, input_data_info=input_data_info,
                      chunck_size='100')
invocation_id = ret$invocation_id
message = ret$message

cat("invocation_id: ", invocation_id, "\n")

# The workflow invocation state that is expected in order to be able to
# continue (ie to identify the output data) is : 'scheduled' terminal state.

# Note : to get more reports from waitWorkflowOutputDataIdent, when calling it
# add input parameter : with_more_reports=TRUE

ret <- waitWorkflowOutputDataIdent(env=siwaaenv, invocation_id=invocation_id)
terminal_state = ret$terminal_state
invocation_scheduled = ret$invocation_scheduled
output_data_identified = ret$output_data_identified
output_data_info = ret$output_data_info
invocation_info = ret$invocation_info
invocation_step_jobs_summary_info = ret$invocation_step_jobs_summary_info
invocation_summary_info = ret$invocation_summary_info
message = ret$message

cat("terminal_state: ", terminal_state, " ** ",
    "invocation_scheduled: ", invocation_scheduled, " ** ",
    "output_data_identified: ", output_data_identified, "\n")

# At this step :
# - If not output_data_identified, then :
#   - If terminal_state and not invocation_scheduled, then STOP (workflow 
#     invocation will never be scheduled).
#   - If not terminal_state, then you can call again
#     'waitWorkflowOutputDataIdent' to continue survey.
# - If output_data_identified, then you can continue (waitSimsResults...).

# Note : to get some job reports from waitSimsResults, when calling it
# add input parameter : with_job_report=TRUE

ret = waitSimsResults(env=siwaaenv, data_info=output_data_info)
output_data_info = ret$data_info # DON'T FORGET to refresh output_data_info !!!
message <- ret$message
end <- ret$end
data_ready <- ret$data_ready

cat("end: ", end, " ** ", "data_ready: ", data_ready, "\n")

# At this step :
# -  If end and not data_ready, then STOP.
# -  If not end, then you can call again 'wait_sim_results' to continue survey
# -  If end and data_ready, then you can download results.

# Download the output data file
ret <- saveResults(env=siwaaenv, data_info=output_data_info)
output_file_path <- ret$filepath
message <- ret$message

if (output_file_path != "") cat("OK") else cat("NOT OK")

# Verify downloaded .RData file
if (output_file_path != ""){
    load(output_file_path)
    lattice::xyplot(SOC~YEAR, groups = ID, data = res, type = "l")
}

ret <- clearHistory(env=siwaaenv, data_info=output_data_info)
history_id <- ret$history_id
history_name <- ret$history_name
purged <- ret$purged
deleted <- ret$deleted
message <- ret$message
```

