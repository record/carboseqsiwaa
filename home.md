# carboseqsiwaa R package

- Branch 'main' (commit short SHA : COMMIT_SHORT_SHA_main)

  - R package [carboseqsiwaa-main.tar.gz](./main/carboseqsiwaa-main.tar.gz) file
  - Online documentation [Reference](./main/reference)
  - Documentation [carboseqsiwaa-main.pdf](./main/carboseqsiwaa-main.pdf) file

- Branch 'dev' (commit short SHA : COMMIT_SHORT_SHA_dev)

  - R package [carboseqsiwaa-dev.tar.gz](./dev/carboseqsiwaa-dev.tar.gz) file
  - Online documentation [Reference](./dev/reference)
  - Documentation [carboseqsiwaa-dev.pdf](./dev/carboseqsiwaa-dev.pdf) file


- Temporary :

  - Branch 'dev-nr' (commit short SHA : COMMIT_SHORT_SHA_devnr)

    - R package [carboseqsiwaa-dev-nr.tar.gz](./dev-nr/carboseqsiwaa-dev-nr.tar.gz) file
    - Online documentation [Reference](./dev-nr/reference)
    - Documentation [carboseqsiwaa-dev-nr.pdf](./dev-nr/carboseqsiwaa-dev-nr.pdf) file

  - ...

